import com.sun.deploy.panel.RadioPropertyGroup;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class GUI extends JFrame {

    private JButton button1 = new JButton("��������� �������");
    private JTextField input1 = new JTextField("", 1);
    private JTextField input2 = new JTextField("", 1);
    private JTextField input3 = new JTextField("", 1);
    private JTextField input4 = new JTextField("", 1);
    private JTextField input5 = new JTextField("", 1);
    private JTextField input6 = new JTextField("", 1);
    private JLabel label1 = new JLabel("������� ����� ������������ �������: ");
    private JLabel label2 = new JLabel("������� ����� ��.����� �����������: ");
    private JLabel label3 = new JLabel("������� ����� ��.����� ����������: ");
    private JLabel label4 = new JLabel("������� ����� ��.����� ���������� � �����: ");
    private JLabel label5 = new JLabel("���� ������: ");
    private JLabel label6 = new JLabel("���������: ");
//    private JCheckBox check1 = new JCheckBox("�������� ����� �� ������� N: �� ����������� ", false);
//    private JCheckBox check2 = new JCheckBox("������� ����� ������ �� ������� N: ��� ������ �� ����������� ", false);


//    private JRadioButton radioEKP = new JRadioButton("���");
//    private JRadioButton radioEKS = new JRadioButton("���");

    public GUI() {
        super("�������� �������� �� ����������");
        this.setBounds(100, 100, 1000, 250);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Container container = this.getContentPane();
        container.setLayout(new GridLayout(8, 2, 2, 2));
        container.add(label1);
        container.add(input1);
        container.add(label2);
        container.add(input2);
        container.add(label3);
        container.add(input3);
        container.add(label4);
        container.add(input4);
        container.add(label5);
        container.add(input5);
        container.add(label6);
        container.add(input6);


        ButtonGroup group = new ButtonGroup();
        button1.addActionListener(new ButtonEventListener());
        container.add(button1);
    }

    class ButtonEventListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String message = "";

            LogicFunctions logic = new LogicFunctions();

            //test Path's
//            String filesPath = "D:\\MyCode\\packagesender\\Data";
//            String senderMail = "pirateussrs@yandex.ru";
//            String recipientMail = "pirateussr@yandex.ru";
//            String recipientInCopyMail = "";
//            String mailBody = "�����";
//            String mailTheme = "����";

            //GUI Path's
            String filesPath = input1.getText();
            String senderMail = input2.getText();
            String recipientMail = input3.getText();
            String recipientInCopyMail = input4.getText();
            String mailTheme = input5.getText();
            String mailBody = input6.getText();


            if (filesPath.length() <= 0 || senderMail.length() <= 0 || recipientMail.length() <= 0) {
                message += "������������ ������ ��� �������!\n��������� ������������ ����� � ��������� �������!";
            } else {
                File packagesPath = new java.io.File(filesPath);
                if (!packagesPath.isDirectory()) {
                    message += "����� ������������ ������ ������ �����������!\n��������� ������������ ����� � ��������� �������!";
                } else {
//                    if (packagesPath.getAbsolutePath().contains(" ")) {
//                        message += "����� ������������ ������ ������ �������� �������!\n������� ����� ��������� ���, ����� ��� �� ��������� ������� � ��������� �������!";
//                    } else {
                    if (!senderMail.contains("@")) {
                        message += "����� �����������: " + senderMail + " �����������!\n��������� ������������ ����� � ��������� �������!\n������: example@mail.ca.sbrf.ru";
                    } else {
                        if (!recipientMail.contains("@")) {
                            message += "����� ����������: " + recipientMail + " �����������!\n��������� ������������ ����� � ��������� �������!\n������: example@mail.ca.sbrf.ru";
                        } else {
                            if (recipientInCopyMail.length() > 0 && !recipientInCopyMail.contains("@")) {
                                message += "����� ���������� � ����� " + recipientInCopyMail + " �����������!\n��������� ������������ ����� � ��������� �������!\n������: example@mail.ca.sbrf.ru";
                            } else {
                                VBSconfig vbsConf = new VBSconfig(packagesPath, senderMail, recipientMail,
                                        recipientInCopyMail, mailBody, mailTheme);
                                logic.sendPackages(vbsConf);
                                message += "������ ������� ����������!";
                            }
                        }
                    }
                }

            }
//            }

            JOptionPane.showMessageDialog(null, message, "Outpoot", JOptionPane.PLAIN_MESSAGE);


        }


    }

}

