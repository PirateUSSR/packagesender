import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class VBSconfig {

     File packagePath = null;
     String senderMail = null;
     String recipientMail = null;
     String recipientInCopyMail = null;
     String mailBody = null;
     String mailTheme = null;
     List noPackageInPath = new ArrayList();

    VBSconfig(File filePath, String senderMail, String recipientMail,
            String recipientInCopyMail, String mailTheme, String mailBody){
        this.packagePath = filePath;
        this.senderMail = senderMail;
        this.recipientMail = recipientMail;
        this.recipientInCopyMail = recipientInCopyMail;
        this.mailTheme = mailTheme;
        this.mailBody = mailBody;
    }
}
