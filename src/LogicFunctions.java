import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class LogicFunctions {

    void sendPackages(VBSconfig vbsConf){
        boolean packageExist = findPackage(vbsConf);

    }

    boolean findPackage(VBSconfig vbSconfig){
        File path = vbSconfig.packagePath;
        boolean packageExist = false;
        File filesList[] = path.listFiles();
        assert filesList != null;
        for (File filename:filesList) {
            if (filename.isDirectory()){
                String dirPath = filename.toString();
//                System.out.println(dirPath);
                File newPath = new java.io.File(dirPath);
                vbSconfig.packagePath = newPath;
                findPackage(vbSconfig);
            } else {
                if (filename.getName().equals("package.rar")){
                    packageExist = true;
                    createVbsFile(vbSconfig);
                    startVbsSender(vbSconfig.packagePath);
//                    System.out.println("The path: "+path+" contains "+filename.getName());
                }
            }
        }

//        if (!packageExist){
//            System.out.println("The file package.rar was not founded in path: "+ path);
//        }
        return packageExist;
    }

    void createVbsFile(VBSconfig vbsConf) {
        String text = "Set oApp = CreateObject(\"outlook.Application\")\r\n" +
                "Set pis = oApp.CreateItem(0)\n" +
                "pis.SentOnBehalfOfName = \""+vbsConf.senderMail+"\"\r\n" +
                "pis.to = \""+vbsConf.recipientMail+"\"\r\n";

        if (!vbsConf.recipientInCopyMail.isEmpty()){
            text += "pis.CC = \""+vbsConf.recipientInCopyMail+"\"\r\n";
        }
        if (!vbsConf.mailTheme.isEmpty()){
            text += "pis.Subject = \""+vbsConf.mailBody+"\"\r\n";
        }
        if (!vbsConf.mailBody.isEmpty()) {
            text += "pis.Body = \""+vbsConf.mailTheme+"\"\r\n";
        }

        text += "pis.Attachments.Add(\""+vbsConf.packagePath+"\\package.rar"+"\")\r\n" +
                "pis.Send";
//        System.out.println(text);

        String filePath = vbsConf.packagePath.toString()+"\\mailSender.vbs";
        File msgFile = new java.io.File(filePath);
        if (msgFile.exists()){
            msgFile.delete();
        }

        appendUsingFileWriter(filePath, text);
    }

    void startVbsSender(File path){
        System.out.println("the path is: " + path);
        File fileName[] = path.listFiles();
        assert fileName != null;
        for (File i : fileName) {
            System.out.println(i.getName());
            if (i.getName().equals("mailSender.vbs")) {
                try {
                    System.out.println(i.toString());
                    Runtime.getRuntime().exec(new String[] {
                            "wscript.exe",
                            i.toString()
                    });
                    System.out.println("The VBS was runned");
                } catch (IOException e) {
                    System.out.println(e);
                    System.exit(0);
                }
            }

        }
    }

    public void appendUsingFileWriter(String filePath, String text) {
        File file = new File(filePath);
        FileWriter fr = null;
        try {
            fr = new FileWriter(file, true);
            fr.write(text);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                assert fr != null;
                fr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
